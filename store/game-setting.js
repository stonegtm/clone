export const state = () => ({
  gamestatus: [],
})

export const mutations = {
  GET_STATUS(state, payload) {
    state.gamestatus = payload.data.data
  },
  UPDATE_STATUS(state, payload) {
    state.gamestatus = payload
  },
}

export const actions = {
  async getStatus({ state, commit }) {
    const res = await this.$axios.$get('/api/v1/games/status')
    commit('GET_STATUS', res)
  },

  async updateStatus({ commit }, data) {
    const update = { games: data }
    const res = await this.$axios.$patch('/api/v1/games/status', update)
    if (res.success) {
      commit('UPDATE_STATUS', data)
    }
    return res
  },
}

export const getters = {
  statusgame(state) {
    return state.gamestatus
  },
}

export const state = () => ({
  agents_list: [],
})

export const mutations = {
  SET_AGENT_LIST(state, payload) {
    state.agents_list = payload
  },
}

export const actions = {
  async setAgentsList({ commit, state, dispatch }, data) {
    if (!data.search) {
      const res = await this.$axios.$get('/api/v1/agents', {
        params: data,
      })
      return res
    } else {
      const res = await this.$axios.$get(`/api/v1/agents/${data.search}`)
      res.data.agnets = [res.data]
      res.data.paging = { count: 1 }
      return res
      // return { data: { company: [res.data], paging: { count: 1 } } }
    }

    // if (!data.search) {
    //   const res = await this.$axios.$get('/api/v1/agents', {
    //     params: data,
    //   })
    //   return res
    // } else {
    //   const res = await this.$axios.$get(
    //     `/api/v1/${data.company}/${data.search}`
    //   )
    //   res.data[data.company] = [res.data]
    //   res.data.paging = { count: 1 }
    //   return res
    //   // return { data: { company: [res.data], paging: { count: 1 } } }
    // }
  },

  async createAgent({ commit }, data) {
    const res = await this.$axios.$post('/api/v1/agents', data)
    return res
  },

  async editAgent({ commit }, data) {
    const edit = {
      name: data.name,
      credit: parseFloat(data.credit),
      shared: parseFloat(data.shared),
      Commission: parseFloat(data.commission),
      commissionSlot: parseFloat(data.commissionSlot),
      minBet: parseFloat(data.minBet),
      maxBet: parseFloat(data.maxBet),
      chipList: data.chipList,
      commissionSport: parseFloat(data.commissionSport),
      slotDisable: data.slotDisable ? 1 : 0,
      sportDisable: data.sportDisable ? 1 : 0,
      casinoDisable: data.casinoDisable ? 1 : 0,
      vendorShared: {
        casino_star: parseFloat(data.vendorShared.casino_star),
        slot_play_n_go: parseFloat(data.vendorShared.slot_play_n_go),
        slot_dragoon: parseFloat(data.vendorShared.slot_dragoon),
        casino_sbo: parseFloat(data.vendorShared.casino_sbo),
        sport_sbo: parseFloat(data.vendorShared.sport_sbo),
        casino_sa: parseFloat(data.vendorShared.casino_sa),
        fishing_dragoon: parseFloat(data.vendorShared.fishing_dragoon),
        slot_joker: parseFloat(data.vendorShared.slot_joker),
        fishing_joker: parseFloat(data.vendorShared.fishing_joker),
        casino_gd88: parseFloat(data.vendorShared.casino_gd88),
      },
    }
    try {
      const res = await this.$axios.$patch(`/api/v1/agents/${data.id}`, edit)
      return res
    } catch (error) {
      return error.message
    }
  },
 async infoEdit({ commit }, data) {
    const res = await this.$axios.$get(`api/v1/agents/${data}`)
    return res
  },
}

export const getters = {
  getAgentsList(state) {
    return state.agents_list
  },
}

import drillDownData from '../resources/drill-down-mockup.json'

export const state = () => ({
  drill_down_list: [],
  status_code: 0,
})

export const mutations = {
  SET_DRILL_DOWN_LIST(state, payload) {
    state.drill_down_list = payload
  },
}

export const actions = {
  async setDrillDownList({ commit, state, dispatch }) {
    await commit('SET_DRILL_DOWN_LIST', drillDownData)
    return drillDownData
  },
}

export const getters = {
  getDrillDownList(state) {
    return state.drill_down_list
  },
}

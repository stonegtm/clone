export const state = () => ({
  token: null,
  expire: null,
  infomation: null,
})

export const mutations = {
  LOGIN(state, payload) {
    state.token = payload.token
    state.expire = payload.expire
  },
  LOGOUT(state) {
    state.token = null
    state.expire = null
  },
  INFO(state, payload) {
    state.infomation = payload
  },
}

export const actions = {
  async login({ commit, state, dispatch }, data) {
    // const res = await this.$axios.$post('/api/v1/login', data)
    // if (res.success) {
    //   commit('LOGIN', res)
    //   this.$cookies.set('What_is_this', res)
    // }
    // return res
    try {
      const res = await this.$axios.$post('/api/v1/login', data)
      if (res.success) {
        commit('LOGIN', res)
        this.$cookies.set('What_is_this', res)
        if(res.success){
          window.location.href = "/";
        }
      }
      return res
    } catch (error) {
      return error.message
    }
  },

  async logout({ commit }) {
    await this.$axios.$get('/api/v1/logout')
    commit('LOGOUT')
    this.$cookies.removeAll()
    window.location.href = "/login";
  },

  addAuth({ commit }, data) {
    commit('LOGIN', data)
  },

  async checkToken({ commit }) {
    try {
      const res = await this.$axios.$get('/api/v1/admin/profile')
      if (res.success) {
        commit('INFO', res.data)
      }
    } catch (error) {
      commit('LOGOUT')
      this.$cookies.removeAll()
    }
  },
}

export const getters = {
  information(state) {
    return state.infomation
    
  },
}

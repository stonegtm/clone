export const state = () => ({
  game: [],
})

export const mutations = {
  GET_GAME(state, payload) {
    state.game = payload
  },
}

export const actions = {
  async getgame({ commit, state, dispatch }, data) {
    if (!data.search) {
      const res = await this.$axios.$get(`/api/v1/${data.company}`, {
        params: data,
      })
      return res
    } else {
      try {
        const res = await this.$axios.$get(
          `/api/v1/${data.company}/${data.search}`
        )
        res.data[data.company] = [res.data]
        res.data.paging = { count: 1 }
        return res
      } catch (error) {
        return error.message
      }

      // return { data: { company: [res.data], paging: { count: 1 } } }
    }
  },

  async createGame(data) {
    const formData = new FormData()
    formData.append('image', data.image, data.image.name)
    formData.append('name', data.name)
    formData.append('desktopGameId', data.dtgame)
    formData.append('mobileGameId', data.mbgame)
    formData.append('desktopGid', data.dtgid)
    formData.append('mobileGid', data.mbgid)
    formData.append('isDeleted', 0)
    formData.append('isNewGame', 0)
    formData.append('isHotGame', 0)
    try {
      const res = await this.$axios.$post(
        `/api/v1/${data[0].company}`,
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        }
      )
      return res
    } catch (error) {
      return error.message
    }
  },
}

export const getters = {}

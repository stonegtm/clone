
export default async function ({ app, store, redirect }) {
    const cookie = await app.$cookies.get('What_is_this')
    if (!cookie) {
      redirect('/login')
    }
    if (cookie) {
      if (!store.state.auth.token) {
        await store.dispatch(
          'auth/addAuth',
          { token: cookie.token ,expire: cookie.expire },
          { root: true }
        )
        await store.dispatch('auth/checkToken', null, { root: true })
      }
    }
  }